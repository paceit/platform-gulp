var config = require('../../../config');
var error = require('../../../util/error');
var gulp = require('gulp');
var minify = require('gulp-clean-css');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('build.css', ['assets.css', 'assets.less', 'assets.sass'], function() {
    var paths = config.get('build.assets.css');

    return gulp.src(paths.src)

    // Minify CSS
    .pipe(minify({rebase: false}))
        .on('error', error)

    // Add to build
    .pipe(gulp.dest(paths.dest));
});