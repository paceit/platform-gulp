var config = require('../../../config');
var error = require('../../../util/error');
var fs = require('fs');
var gulp = require('gulp');
var rev = require('gulp-rev');
var through = require('through2');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('build.version', function() {
    var paths = config.get('build');

    return gulp.src(paths.src)

    // Revision files
    .pipe(rev())
        .on('error', error)

    // Add to build
    .pipe(gulp.dest(paths.dest))

    // Remove original files
    .pipe(removeOrig())

    // Add manifest
    .pipe(rev.manifest())
        .pipe(gulp.dest(paths.dest))
});

// Remove original build files
var removeOrig = function() {
    return through.obj(function(file, enc, cb) {
        if (file.revOrigPath) {
            fs.unlink(file.revOrigPath);
        }

        this.push(file);
        return cb();
    });
};