var config = require('../../../config');
var gulp = require('gulp');
var error = require('../../../util/error');
var uglify = require('gulp-uglify');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('build.js', ['assets.coffee', 'assets.js', 'assets.views'], function() {
    var paths = config.get('build.assets.js');

    return gulp.src(paths.src)

    // Minify scripts
    .pipe(uglify())
        .on('error', error)

    // Add to build
    .pipe(gulp.dest(paths.dest));
});