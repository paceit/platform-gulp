var config = require('../../../config');
var gulp = require('gulp');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('build.fonts', ['assets.fonts'], function() {
    var paths = config.get('build.assets.fonts');

    return gulp.src(paths.src)

    // Add to build
    .pipe(gulp.dest(paths.dest));
});