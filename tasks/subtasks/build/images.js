var config = require('../../../config');
var gulp = require('gulp');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('build.images', ['assets.images'], function() {
    var paths = config.get('build.assets.images');

    return gulp.src(paths.src)

    // Add to build
    .pipe(gulp.dest(paths.dest));
});