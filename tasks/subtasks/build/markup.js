var config = require('../../../config');
var gulp = require('gulp');
var error = require('../../../util/error');
var htmlmin = require('gulp-htmlmin');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('build.markup', ['assets.markup'], function(cb) {
    var paths = config.get('build.assets.markup');
    var markup = require('../../../util/markup');

    // Skip if we aren't processing markup
    if (markup === false) {
        cb();
        return;
    }

    return gulp.src(paths.minifySrc)

    // Minify markup
    .pipe(htmlmin({ collapseWhitespace: true }))
        .on('error', error)

    // Add to build
    .pipe(gulp.dest(paths.dest));
});