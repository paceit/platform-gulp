var config       = require('../../../config');
var gulp         = require('gulp');
var handleErrors = require('../../../util/error');
var merge        = require('merge-stream');
var revReplace   = require('gulp-rev-replace');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('build.paths', function() {
    var dest, i, paths = [], streams = [];

    // Parse CSS paths
    dest = config.get('build.assets.css.dest');
    paths.push({
        src:    dest + '/**',
        dest:   dest,
        prefix: '',
    });

    // Parse JS paths
    dest = config.get('build.assets.js.dest');
    paths.push({
        src:    dest + '/**',
        dest:   dest,
        prefix: config.get('build.prefix'),
    });

    // Parse markup paths
    dest = config.get('build.assets.markup.dest');
    paths.push({
        src:    dest + '*.{html,json,php,txt,xml}',
        dest:   dest,
        prefix: config.get('build.prefix'),
    });

    // Parse static paths
    dest = config.get('build.assets.static.dest');
    paths.push({
        src:    dest + '/**',
        dest:   dest,
        prefix: config.get('build.prefix'),
    });

    // Process paths
    for (i = 0; i < paths.length; i++) {
        streams.push(
            gulp.src(paths[i].src)

            // Repace paths
            .pipe(revReplace({
                manifest: gulp.src(config.get('build.dest') + '/rev-manifest.json'),
                prefix:   paths[i].prefix
            }))
            .on('error', handleErrors)

            // Overwrite original files
            .pipe(gulp.dest(paths[i].dest))
        );
    }

    // Return merged streams
    return merge.apply(this, streams);
});