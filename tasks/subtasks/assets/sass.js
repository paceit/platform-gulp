var autoprefixer = require('gulp-autoprefixer');
var browser = require('browser-sync');
var config = require('../../../config');
var error = require('../../../util/error');
var gulp = require('gulp');
var include = require('gulp-include');
var merge = require('merge-stream');
var sass = require('gulp-dart-sass');
var sourcemaps = require('gulp-sourcemaps');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('assets.sass', function() {
    var streams = [];

    // Loop through folders
    config.get('assets.sass').forEach(function(paths) {
        streams.push(
            gulp.src(paths.src)

            // Add includes
            .pipe(include())
            .on('error', error)

            // Start sourcemaps
            .pipe(sourcemaps.init())

            // Compile SASS
            .pipe(sass({
                indentedSyntax: false
            }))
            .on('error', error)

            // Add browser prefixes
            .pipe(autoprefixer({
                browsers: ['last 2 version']
            }))
            .on('error', error)

            // Create sourcemaps
            .pipe(sourcemaps.write())

            // Add to build
            .pipe(gulp.dest(paths.dest))

            // Reload browser
            .pipe(browser.reload({
                stream: true
            }))
        );
    });

    return merge.apply(this, streams);
});