var browser = require('browser-sync');
var coffee = require('gulp-coffee');
var config = require('../../../config');
var error = require('../../../util/error');
var gulp = require('gulp');
var include = require('gulp-include');
var merge = require('merge-stream');
var sourcemaps = require('gulp-sourcemaps');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('assets.coffee', function() {
    var streams = [];

    // Loop through folders
    config.get('assets.coffee').forEach(function(paths) {
        streams.push(
            gulp.src(paths.src)

            // Add includes
            .pipe(include())
            .on('error', error)

            // Start sourcemaps
            .pipe(sourcemaps.init())

            // Compile coffeescripts
            .pipe(coffee())
            .on('error', error)

            // Create sourcemaps
            .pipe(sourcemaps.write())

            // Add to build
            .pipe(gulp.dest(paths.dest))

            // Reload browser
            .pipe(browser.reload({
                stream: true
            }))
        );
    });

    return merge.apply(this, streams);
});