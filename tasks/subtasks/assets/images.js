var browser = require('browser-sync');
var config = require('../../../config');
var error = require('../../../util/error');
var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var merge = require('merge-stream');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('assets.images', function() {
    var streams = [];

    // Loop through folders
    config.get('assets.images').forEach(function(paths) {
        streams.push(
            gulp.src(paths.src)

            // Optimise images
            .pipe(imagemin())
            .on('error', error)

            // Add to build
            .pipe(gulp.dest(paths.dest))

            // Reload browser
            .pipe(browser.reload({
                stream: true
            }))
        );
    });

    return merge.apply(this, streams);
});