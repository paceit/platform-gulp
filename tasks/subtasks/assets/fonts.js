var browser = require('browser-sync');
var config = require('../../../config');
var gulp = require('gulp');
var merge = require('merge-stream');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('assets.fonts', function() {
    var streams = [];

    // Loop through folders
    config.get('assets.fonts').forEach(function(paths) {
        streams.push(
            gulp.src(paths.src)

            // Add to build
            .pipe(gulp.dest(paths.dest))

            // Reload browser
            .pipe(browser.reload({
                stream: true
            }))
        );
    });

    return merge.apply(this, streams);
});