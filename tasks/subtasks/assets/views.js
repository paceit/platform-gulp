var babelify = require('babelify');
var browser = require('browser-sync');
var browserify = require('browserify-incremental');
var config = require('../../../config');
var error = require('../../../util/error');
var fs = require('fs');
var gulp = require('gulp');
var latest = require('babel-preset-latest');
var merge = require('merge-stream');
var react = require('babel-preset-react');
var source = require('vinyl-source-stream');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('assets.views', function(cb) {
    var es6 = require('../../../util/es6');
    var views = require('../../../util/views');
    var presets, streams = [];

    // Skip if we aren't processing views
    if (views === false) {
        cb();
        return;
    }

    // Build presets
    presets = [react];
    if (es6) {
        presets.push([latest, {loose: true}]);
    }

    // Loop through folders
    config.get('assets.views').forEach(function(paths) {

        // Check path exists
        if (! fs.existsSync(paths.src)) {
            return;
        }

        // Add to streams
        streams.push(
            browserify({
                cacheFile: './browserify-cache.json',
                entries: paths.src,
                transform: [
                    [babelify, {
                        global:  true,
                        ignore:  /\/node_modules\/(?!platform-.*\/)/,
                        presets: presets
                    }]
                ]
            })

            // Expose platform
            .require('platform-react', {
                expose: 'platform'
            })

            // Bundle build
            .bundle()
            .on('error', error)
            .pipe(source('bundle.js'))

            // Add to build
            .pipe(gulp.dest(paths.dest))

            // Reload browser
            .pipe(browser.reload({
                stream: true
            }))
        );
    });

    return merge.apply(this, streams);
});