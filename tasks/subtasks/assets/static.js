var browser = require('browser-sync');
var config = require('../../../config');
var error = require('../../../util/error');
var gulp = require('gulp');
var include = require('gulp-include');
var merge = require('merge-stream');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('assets.static', function() {
    var streams = [];

    // Loop through folders
    config.get('assets.static').forEach(function(paths) {
        streams.push(
            gulp.src(paths.src)

            // Add includes
            .pipe(include())
            .on('error', error)

            // Add to build
            .pipe(gulp.dest(paths.dest))

            // Reload browser
            .pipe(browser.reload({
                stream: true
            }))
        );
    });

    return merge.apply(this, streams);
});