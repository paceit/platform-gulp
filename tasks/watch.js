var config = require('../config');
var gulp = require('gulp');
var watch = require('gulp-watch');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('watch', function(cb) {
    var asset, assets, i, key;

    // Loop through assets
    assets = config.get('assets');
    for (key in assets) {

        // Skip if prototype
        if (!assets.hasOwnProperty(key)) {
            continue;
        }

        watchAsset(key, assets[key]);
    }

    cb();
});

// Watch assets
var watchAsset = function(key, paths) {
    paths.forEach(function(paths) {
        watch(paths.watch, function() {
            config.setGetPackage(paths.package);
            gulp.start(paths.task);
        });
    });
};