var config = require('../config');
var del = require('del');
var gulp = require('gulp');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('destroy', function(cb) {
    var laravel = require('../util/laravel');
    var path = config.get('dest');

    // If we are processing laravel, cleanup build folders
    if (laravel) {
        path = [
            config.get('build.src'),
            config.get('build.assets.css.src'),
            config.get('build.assets.fonts.src'),
            config.get('build.assets.images.src'),
            config.get('build.assets.js.src'),
            config.get('build.assets.static.src'),

            // Don't remove gitignore in build folder
            '!' + config.get('build.dest'),
            '!.gitignore',
        ]
    }

    del(path).then(function() {
        cb();
    });
});