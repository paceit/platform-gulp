var browser = require('browser-sync');
var config = require('../config');
var fs = require('fs');
var gulp = require('gulp');
var path = require('path');
var url = require('url');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('browser', function(cb) {
    var laravel = require('../util/laravel');
    var views = require('../util/views');

    // If we are processing laravel, proxy
    // the configured host
    if (laravel) {
        browser({
            proxy: config.get('laravel.host')
        });
    }

    // Otherwise serve files from build folder
    else {
        browser({
            server: {
                baseDir: config.get('dest'),

                // If we are processing views, make sure
                // any unknown paths use the main index
                // file
                middleware: function(req, res, next) {
                    var exists, file, path;

                    // Skip if we aren't processing views
                    if (! views) {
                        return next();
                    }

                    // Parse filename
                    file = url.parse(req.url);
                    file = file.href.split(file.search).join('');

                    // Check if file exists
                    path   = config.get('output.temp') + file;
                    exists = fs.existsSync(path);

                    // If file does not exist and we are not the browsersync
                    // client, set request to default file
                    if (! exists && file.indexOf('browser-sync-client') === -1) {
                        req.url = '/index.html';
                    }

                    return next();
                }
            }
        });
    }

    cb();
});