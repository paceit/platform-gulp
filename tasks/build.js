var gulp = require('gulp');
var run = require('run-sequence');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

gulp.task('build', ['destroy'], function() {
    var minify = require('../util/minify');

    // Build minified assets
    if (minify) {
        return run.apply(this, [
            ['build.css', 'build.fonts', 'build.images', 'build.js', 'build.markup', 'build.static'],
            'build.version',
            'build.paths'
        ]);
    }

    // Build assets
    return run.apply(this, [
        [
            'assets.coffee',
            'assets.css',
            'assets.fonts',
            'assets.images',
            'assets.js',
            'assets.less',
            'assets.markup',
            'assets.sass',
            'assets.static',
            'assets.views'
        ]
    ]);
});