var config = require('./config');
var extend = require('extend');
var gulp = require('gulp');
var requireDir = require('require-dir');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

// Print header
process.stdout.write('\x1b[2m\
/**\n\
 * Platform: Gulp Framework\n\
 *\n\
 * @copyright ' + new Date().getFullYear() + ' Pace IT Systems Ltd\n\
 * @author    Pace IT Systems Ltd\n\
 * @license   Proprietary\n\
 */\
\x1b[0m\n\n');

// Load tasks
requireDir('./tasks', { recurse: true });

// Create default task
gulp.task('default', ['build'], function() {
    gulp.start(['watch', 'browser']);
});

// Export functions
module.exports = {

    // Set arguments
    args: function(args) {
        this.config({
            args: args
        });
    },

    // Override configuration values
    config: function(merge, data) {
        config.extend(merge, data);
    },

    // Set packages
    packages: function(packages) {
        this.config({
            packages: packages
        });
    }
};