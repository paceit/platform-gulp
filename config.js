var extend = require('extend');

/**
 * Pace: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

// This is the default configuration used. Any config options
// passed when requiring the package will be merged with these
// existing values.
var config = {

    // Default arguments
    args: [],

    // Laravel
    laravel: {
        host: 'docker'
    },

    // Output folders
    output: {
        laravel: 'public',
        static: 'dist',
        temp: '.tmp'
    },

    // Package overrides
    overrides: {},

    // Packages
    packages: [],

    // Asset directories
    assets: {
        coffee: {
            watch: 'resources/assets/coffee/**/*.coffee',
            src: 'resources/assets/coffee/**/[^_]*.coffee',
            dest: 'js'
        },
        css: {
            src: 'resources/css/**/*.css',
            dest: 'css'
        },
        fonts: {
            src: 'resources/fonts/**/*.{eot,ttf,otf,cff,afm,lwfn,ffil,fon,pfm,pfb,woff,woff2,svg,std,pro,xsf}',
            dest: 'fonts'
        },
        images: {
            src: 'resources/images/**/*.{gif,ico,jpeg,jpg,png,svg}',
            dest: 'images'
        },
        js: {
            watch: 'resources/js/**/*.{js,jsx}',
            src: 'resources/js/**/[^_]*.{js,jsx}',
            dest: 'js'
        },
        less: {
            watch: 'resources/assets/less/**/*.less',
            src: 'resources/assets/less/**/[^_]*.less',
            dest: 'css'
        },
        markup: {
            watch: 'resources/markup/**/*.{html,json,php,txt,xml}',

            // Purposely not doing a recursive match, don't want
            // to delete text files etc put in other public folders
            src: 'resources/markup/[^_]*.{html,json,php,txt,xml}',
            dest: ''
        },
        sass: {
            watch: 'resources/assets/sass/**/*.{sass,scss}',
            src: 'resources/assets/sass/**/[^_]*.{sass,scss}',
            dest: 'css'
        },
        static: {
            src: 'resources/static/**/[^.]*',
            dest: 'static'
        },
        views: {
            watch: 'resources/{react,views}/**/*.{js,jsx}',
            src: 'resources/react/Platform.jsx',
            dest: 'js/react'
        }
    },

    // Build directories
    build: {
        src: 'build/**',
        dest: 'build',
        prefix: 'build/',

        assets: {
            css: {
                src: 'css/**/*.css',
                dest: 'build/css'
            },
            fonts: {
                src: 'fonts/**/*.{eot,ttf,otf,cff,afm,lwfn,ffil,fon,pfm,pfb,woff,woff2,svg,std,pro,xsf}',
                dest: 'build/fonts'
            },
            images: {
                src: 'images/**/*.{gif,ico,jpeg,jpg,png,svg}',
                dest: 'build/images'
            },
            js: {
                src: 'js/**/*.{js,jsx}',
                dest: 'build/js'
            },
            markup: {

                // Source to use when minifying
                minifySrc: '*.html',

                src: '*.{html,json,php,txt,xml}',
                dest: ''
            },
            static: {
                src: 'static/**/[^.]*',
                dest: 'build/static'
            },
        }
    }
};

// Export functions
module.exports = {

    // Package to use for next get call
    getPackage: undefined,

    // Override configuration values
    extend: function(merge, data) {
        var package;

        // Skip processing if we havn't actually
        // been passed any values to merge
        if (!merge) {
            return;
        }

        // If we have data, switch into config
        // overrides format
        if (data) {
            package = merge;
            merge = { overrides: {} };

            // Add data to overrides
            merge.overrides[package] = data;
        }

        // Merge into config
        config = extend(true, config, merge);
    },

    // Return configuration values
    get: function(key) {
        var data = config,
            i, length;

        // If we have no destination folder, this must be first call
        // to retrieve configuration values. Run function to actually
        // parse config and set real paths
        if (!config.dest) {
            parseConfig();
        }

        // If we havn't been passed a key, return full config
        if (!key) {
            return data;
        }

        // Retrieve actual config
        key = key.split('.').forEach(function(key) {
            data = data[key];
        });

        // If we are only retieving a singular package,
        // loop to find correct config
        if (this.getPackage !== undefined) {
            length = data.length;

            // Loop through array
            for (i = 0; i < length; i++) {
                if (data[i].package === this.getPackage) {
                    data = [data[i]];
                    break;
                }
            }

            this.getPackage = undefined;
        }

        // Return values
        return data;
    },

    // Return whether platform has argument
    hasArg: function(arg) {
        var found = false;

        // Is argument set as default?
        if (config.args.indexOf(arg) !== -1) {
            return true;
        }

        // Attempt to detect argument from process
        process.argv.every(function(a) {
            if (a.substr(2) === arg) {
                found = true;
                return false;
            }

            return true;
        });

        return found;
    },

    // Set package to use for next get call
    setGetPackage: function(package) {
        this.getPackage = package;
    }
};

// Loop through config and set real paths
function parseConfig() {
    var arr, asset, i, key, package;

    // Set destination folder
    config.dest = require('./util/destination')(
        config.output.laravel,
        config.output.static,
        config.output.temp
    );

    // Parse assets
    for (key in config.assets) {
        arr = [];

        // Skip if prototype
        if (!config.assets.hasOwnProperty(key)) {
            continue;
        }

        asset = config.assets[key];
        asset.watch = asset.watch || asset.src;
        asset.task = asset.task || 'assets.' + key;

        // Add root
        arr.push({
            package: false,
            watch: asset.watch,
            task: asset.task,
            src: asset.src,
            dest: config.dest + '/' + asset.dest
        });

        // Loop through packages
        config.packages.forEach(function(package) {
            var overrides = {};

            // Do we have any overrides?
            if (config.overrides[package] && config.overrides[package].assets && config.overrides[package].assets[key]) {
                overrides = config.overrides[package].assets[key];
            }

            arr.push({
                package: package,
                watch: package + '/' + (overrides.watch || asset.watch),
                task: overrides.task || asset.task,
                src: package + '/' + (overrides.src || asset.src),
                dest: config.dest + '/' + (overrides.dest || asset.dest) + '/' + package
            });
        });

        // Write back to config
        config.assets[key] = arr;
    }

    // Parse build
    config.build.src = config.dest + '/' + config.build.src;
    config.build.dest = config.dest + '/' + config.build.dest;

    for (key in config.build.assets) {

        // Skip if prototype
        if (!config.build.assets.hasOwnProperty(key)) {
            continue;
        }

        asset = config.build.assets[key];
        asset.src = config.dest + '/' + asset.src;
        asset.dest = config.dest + '/' + asset.dest;

        // Add minify source
        if (asset.minifySrc !== undefined) {
            asset.minifySrc = config.dest + '/' + asset.minifySrc;
        }
    }
}