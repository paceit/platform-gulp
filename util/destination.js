var config = require('../config');
var laravel = require('./laravel');

/**
 * Pace: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

module.exports = function(laravelDest, static, temp) {
    var minify = false;

    // If we are processing laravel, use
    // laravel directory
    if (laravel) {
        return laravelDest;
    }

    return config.hasArg('minify') ? static : temp;
};