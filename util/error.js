var browser = require('browser-sync');
var notify  = require('gulp-notify');

/**
 * Pace: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

module.exports = function() {
    var args = Array.prototype.slice.call(arguments);

    // Send error to browser
    browser.notify('Platform error, check log', 5000);

    // Send error to notification centre
    notify.onError({
        title:   'Platform Error',
        message: '<%= error %>'
    }).apply(this, args);

    // Keep gulp from hanging on this task
    this.emit('end');
};
