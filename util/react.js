/**
 * Pace: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

// Default to false
module.exports = false;

// Detect laravel argument
process.argv.every(function (arg) {
	if (arg === '--laravel' || arg === '--static') {
		module.exports = false;
		return false;
	} else if (arg === '--react') {
		module.exports = true;
		return false;
	}

    return true;
});
