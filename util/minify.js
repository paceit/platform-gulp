var config = require('../config');

/**
 * Pace: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

module.exports = config.hasArg('minify');

// If we are minifying, make sure node environment
// is set to production
if (module.exports === true) {
    process.env.NODE_ENV = 'production';
}