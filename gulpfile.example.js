var platform = require('platform-gulp');

/**
 * Platform: Gulp Framework
 *
 * @copyright 2015 Pace IT Systems Ltd
 * @author    Pace IT Systems Ltd
 * @license   Proprietary
 */

// Set packages
platform.packages([
    'packages/my/app'
]);

// Override config values
platform.config({
    laravel: {
        host: 'my.host'
    }
});